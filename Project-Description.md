# MuteBox

## Team Members
Trent Slavik (CS): slaviktm@mail.uc.edu

Andrew Dillon (CS): dillonal@mail.uc.edu

Timothy Holstein (CS): holstetr@mail.uc.edu

## Project Topic Area
This project is a combination of multiple different topic areas, including:
- Machine Learning
- Signal Processing
- Embedded Systems/Hardware Design

## Project Abstract
Our goal is to build a replicable device which will accurately, consistently and quickly mute a television whenever an advertisement is playing, and unmute it when the advertisement concludes. This device should also work without user intraction once configured. In order to achieve this, this device will accomplish goals in different categories: physical specifications, response time, and usability.

## Inadequancy of Current Solutions
When looking at the products that are or were available, each one had at least one of these two flaws: it was not made available for commercial reproduction or it was extremetly rudementry and inaccurate. As more and more television services moved to web based platforms (i.e. streaming), the development of solutions to this problem slowed down. While many people now use these streaming services, many individuals and entities still need a solution to stop commercials while using cable/satilite television.

## Technical Background
For the project topic areas explored in the development of this product, none of the team members have indepth technical knowledge. Teams members have cursory knowledge of machine learning, signal processing, and embedded systems from prior classes and co-ops, but new learning and exploration will need to be done in order to make this project feasible.

## Team Approach to Problem
Our team will be focusing on creating three main subsystems for this project: an embedded system, a machine learning/neural network for signal processing, and a scheduling system. The embedded system will connect to the end point device (television) and run the audio filter created from the machine learning algorithm. A scheduling system will also reside on the embedded system to process the audio when it comes into the device. From here, if a commerical is detected a signal will be sent to the television from an IR sensor on the device to mute, and when the program is detected a signal will be sent back to unmute from the IR sensor.
