# **Trent Slavik**
*Contact: slaviktm@mail.uc.edu*

---
### **Co-op Work Experience**

##### Northrop Grumman | Cincinnati, OH | Cyber Software Engineer Co-op
###### *January 2022 - August 2022*
*	Integrated an internal testing product to be ran within Jenkins to increase testing efficiency
*	Configured multiple virtual machines for development and testing
*   Presented capabilities of our product in customer presentations 

##### Northrop Grumman | Cincinnati, OH | Cyber Software Engineer Co-op
###### *May 2021 - August 2021*
*	Developed a guided user interface application using Angular, HTML, and CSS
*	Created automated tests by implementing tools such as Jest, Storybook, and Cypress

##### Northrop Grumman | Cincinnati, OH | Cyber Software Engineer Co-op
###### *September 2021 - January 2021*
*	Developed applications using the Angular TypeScript framework
*	Built a CI/CD automated pipeline in Jenkins to speed up production processes

##### Northrop Grumman | Cincinnati, OH | Cyber Software Engineer Co-op
###### *January 2020 - May 2020*
*	Completed Java development tasks on a multi-VM automated testing framework
*	Implemented multiple proof of concept mixed reality applications using Unity
*	Presented newly developed applications to senior leaders  

---
### **Technical Skills**
* Programming: C++, C, Java, Python, Typescript 
* Operating Systems: Windows/Linux
* Web Development: HTML, CSS, Angular
* Other: Git, Confluence, JIRA, CI/CD, Jenkins, Cypress, Unity 

---
### **Types of Projects Sought**
* Anything in the realm of low level reverse engineering 
* Mixed reality application for placing furniture within a house
* AI facial reconigion application that unlocks a door based on appropriate privileges  











