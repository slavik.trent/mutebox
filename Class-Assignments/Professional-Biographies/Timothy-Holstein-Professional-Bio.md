﻿# Professional Biography



## Contact Information

- Email: robbyholstein@gmail.com
- School email: holstetr@mail.uc.edu
- Phone: 1-937-532-4360

## COOP Work Experience

### Northrup Grumman Xetron Mission Systems: Rogue Squirrel Team                                          
- Work Specifics are proprietary. Ask NG contact for further details.

- Northrop Grumman Xetron Mission Systems: BITS Team
- Work specifics are classified, ask for NG contact for further details.
- GWT and JavaScript frontend work
- Ruby and Python backend work
- Docker, Kubernetes and containerization work
- Nagios and monitoring work

### Northrop Grumman Xetron Mission Systems: NEPTUNE Team		                     
- Work specifics are proprietary. Ask for NG contact for further details.
- Docker and containerization development and networking
- JavaScript webdev work

### Northrop Grumman Xetron Mission Systems: GIT/ITA Team	
- Full stack development of file system tool
- Full stack development
- Javascript/HTML frontend, Python backend
React, Electron, NodeJs and Django
- File system, scripting and I/O
- Security feature development, including authentication and input sanitization, etc

### Northrop Grumman Xetron Mission Systems: Saturn Sky Team
- Work is classified, contact NG for further details
- Worked extensively with Python and GIT, and C

## Skillset
- Interpersonal Communication
- Webdev, react, NodeJS and Django
- GUI and Front end experience
- Database and backend experience
- Problem solving and critical thinking skills
- Decision making and leadership skills
- JavaScript, Java, C++, MATLAB and Python, RUBY, GWT
- Extensive Networking experience with Cisco devices and IOS.
- C and low level coding experience
- Docker and containerization, software development, Nagios monitoring
- Git development and testing, along with Ansible scripting
- Programming training and knowledge in HTML 
- Networking and outreach skills
- Linux Kernel customization and compilation
- Troubleshooting and analyzing bugs and weaknesses
- Time management and skill under stress



## Project Sought
- The Capstone Project I am seeking is one in which I will be able to expand my relevant knowledge for my field based on what I have observed in my 5 coop rotations.
- Most notably, I would like to gain further experience in reverse engineering, low level and assembly code, and binary analysis. This is by far the most relevant skill set in my field that I have surprisingly little knowledge about, and not much on hand experience.
- To this end, I am seeking a project wherein there will be aspects and hands on use of low level and assembly to create a tangible product, wherein I can track the actual effects of assembly and low level.
- In addition, whilst not required, a keystone project that has use of IOT technology and IOT security would be a welcome addition. However, this comes secondary to me to low level and assembly, but it is worth considering
- Finally, I would of course like the finale of the keystone project to culminate in a tangible piece of code that can be used for job searching and resume purposes. Something that can be, for lack of a better phrase, used to show an employer my knowledge in the field. 

