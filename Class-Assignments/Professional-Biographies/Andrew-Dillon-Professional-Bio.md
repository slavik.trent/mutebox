## Professional Biography: Andrew Dillon

---

## Contact Information
- ### Email: <dillonal@mail.uc.edu>
- ### Phone #: (937) 956-1514

---

## Skills/Expertise
- Pragramming Languages: Java, C++, C
- Operating Systems: Linux, Windows
- Testing: JUnit, Selenium, Robot Framework
- Automation: Ansible, Bash Scripting, Gitlab CI/CD, Docker

---

## Co-op / Other Related Experiences
- Cyber Software Engineer at Northrop Grumman (5 Semesters)
    - Semester #4-#5
        - Automated deployment of testing environment using Ansible
        - Performed C unit/system testing and fixed defects that arose as a result of testing
        - Integrated docker into existing systems for consistency and ease of use
        - Implemented GitLab CI Pipeline and GitLab runners for code maintainability
    - Semester #3
        - Participated in the planning and organization of the technical side of a new project
        - Developed bash scripts for the automation and deployment of tasks/procedures
        - Upgraded and integrated new features into existing software to meet new customer needs
    - Semester #2
        - Developed and implemented automated tests using Selenium and Robot Fra
        - Performed comprehensive research on Java VM profilers for system integration and metrics collection
        - Documented and presented research findings through wiki articles
        - Integrated Elastic Alerting as a new feature for customer and developer use
    - Semester #1
        - Constructed and implemented Java unit tests for a large scale automated system
        - Fixed code base issues and bugs using unit tests
        - Conducted team meetings for planning and peer code review
        - Taught peers about system part interactions and layout

---

## Areas of Intrest
1. Embedded Systems
2. Reverse Engineering
3. Architecture/Software Emulation
4. Offensive and defensive cyber security (red team/blue team)

---

## Type of Project Sought
- Retro gaming console / hardware emulator
- Blockchain exploitation and research
- IoT defense monitoring system for household appliances and applications