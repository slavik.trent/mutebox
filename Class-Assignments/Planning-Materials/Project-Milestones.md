# Project Milestones

**Preliminary Investigation and Research:** For this milestone we will begin to investigate the specific topic areas of our project to begin finding relevant materials and articles related to our project design. Some relevant search topics have been (AI, databases, training algorithms, raspberry pi, IR transmitter and receiver, etc).  

**Initial Hardware Prototype:** This milestone entails collecting all the necessary hardware parts and constructing the initial prototype of the MuteBox. This phase will most likely be as simple as attaching all the necessary third party parts to the Raspberry Pi, and testing them to make sure they work and interact correctly.

**Machine Learning Algorithm Finalization:** Determine based on testing the best way algorithm to make our AI the most accurate when training our scheduling filter.

**Development - Audio Processing Software Executable:** A software executable will be required to run on the Raspberry Pi to accomplish the following objectives: schedule the collection of audio stream in real time, run the audio fingerprinting service (i.e. apply the filter to the audio stream), and send the IR signals to the TV. These different services will be written in Python (ML) and C (scheduler & IR transmission).

**Development - Database Infrastructure:** A working classification system containing each of the used clips for the training set sorted by type (transition), date and/or channel. Maybe a file system or sql-esque data system.

**Development - Transition Filtering:** A filter will be required in order to fingerprint the audio stream coming into the device. This filter will be the product of training a machine learning algorithm with a plethora of tagged audio samples. This filter will be trained off-site the Raspberry Pi, but the final product will be implemented into the fingerprinting executable.

**Machine Learning Training:** This milestone will focus on continually collecting data and adding it to our training set and fine tuning our machine learning algorithm to determine the differences between broadcast and commercial breaks.

**Finalize Hardware Design:** Based on how much throughput our hardware can handle we will need to finalize our hardware design to be able to mute and unmute our sensors in the most efficient, portable, and cost effective manner.

**Testing:** This milestone largely focuses on getting the mute timeframe down to the target level, that being a second. Success is when the product successfully recognizes a commercial is present and mutes itself within a second of time, as well as reenables sound when the programming returns on a consistent basis.

**Product Delivery:** The final completed product will consist of a singular attachable device with a raspberry pi, infrared transmitter and the filter attached to it, that will connect to the television, read its audio inputs, and then mute the television once ads appear.