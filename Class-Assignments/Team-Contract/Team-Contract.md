# CS Senior Design Team Contract

Gitlab Repo: https://gitlab.com/slavik.trent/mutebox

## Team Members
Andrew Dillon
- Email: dillonal@mail.uc.edu
- Phone: 937-956-1514

Trent Slavik
- Email: slaviktm@mail.uc.edu
- Phone: 740-803-2605

Timothy Holstein
- Email: holstetr@mail.uc.edu
- Phone: 937-532-4360

## Project Focus
The focus of our senior design project is to develop an embedded system to automatically detect advertisements playing on a television, and to mute the tv for the duration of the ad. We will accomplish this by utilizing a raspberry pi and custom software to detect when the advertisement will appear via audio fingerprinting. Based on this detection, a scheduler will be implemented to determine if a commercial is playing and unmute the tv when the commercial break is over.

## Project Roles

Andrew Dillon: 
- Task Admin, Developer

Trent Slavik:
- Scrum Master, Developer

Timothy Holstein: 
- Archivist/Report Author, Developer

Whilst each member does have individual roles as specified above, by and large each team member will have the same role. We have worked together long enough professionally and privately that we are comfortable with there being no distinct roles for the actual work itself. The programming, research and development will be handled with an agile methodology based on each member's expertise and interest. Each team member will be required to complete their tasks assigned to them during sprint planning on a weekly basis.

## Expected Time Commitment

We will plan on meeting three times a week: Monday, Wednesday, and Friday. The Monday and Friday meetings will be from 20-60 minutes minimum. The Wednesday meeting will also include our advisor Prof. Gallagher for input, discussion, and direction. The goal of these meetings is to check in on what our teammates are working on and discuss the current status of each member. We will have a longer session, usually an hour and a half at least, on Wednesday wherein we will lay down tasks, give direction on current work, as well as plan for future milestones and piece together substantial work. In addition, it is expected that each team member be flexible for possible weekend work. Regardless of when and how often we meet in a week, it is expected that each team member will be up to date and in contact with the others, checking the discord, gitlab and text chat daily.
