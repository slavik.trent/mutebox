# Task List

1. Investigate computing device capabilities - Trent Slavik

2. Investigate capturing audio/video samples - Robby Holstein 

3. Research audio engineering - Andrew Dillon 

4. Obtain specific hardware (Raspberry Pi, IR receiver, RCA connections, Wiring) - Trent Slavik

5. Investigate what database we should use to ingest our audio data - Andrew Dillon

6. Implement database to ingest commercial transitions - Andrew Dillon

7. Configure all the hardware pieces together to be able receive signal and audio data - Trent Slavik

8. Investigate machine learning algorithms - Andrew Dillon 

9. Automate the collection of commercial transitions - Robby Holstein

10. Setup Database - Robby Holstein

11. Create Design Diagrams for Raspberry Pi Setup - Trent Slavik

12. Set-up Intial Raspberry Pi Configuration V1 - Trent Slavik

13. Test Raspberry Pi Hardware Components - Trent Slavik

14. Investigate DejaVu (python audio fingerprinting) library - Andrew Dillon 

15. Record baseline transition audio sample properties - Robby Holstein

16. Test IR Transmitter / Receiver - Trent Slavik

17. Test audio to ensure it is getting captured on Raspberry Pi - Trent Slavik

18. Obtain storage units - Robby Holstein

19. Test Screen Recording program/television - Robby Holstein

20. Investigate capture automation scripts - Robby Holstein

21. Decide on which channels to capture in which format - Robby Holstein

22. Investigate Multiple stream capture - Robby Holstein

23. Coordinate sharing recording duties - Robby Holstein

24. Investigate Silent recordings - Robby Holstein

25. Figure out how to watch cable tv on computer - Andrew Dillon    

26. Deep learning comparision list - Andrew Dillon

27. Install and play with basic deep learning algorithm - Andrew Dillon

28. Obtain research prototype laptop from Prof. Gallagher

29. Get a large scale recording sucessfully cut up into transitions and false positives. Pass into Audacity for analysis

30. Initial testing to see the baseline capabilities of the Raspberry Pi

31. Assemble: package list, audio processing example code bases

32. Automated recording and editing process finalized

33. Finalize machine learning (ML) algorithm + have some input testing

34. Hardware parts tested

35. Document containing test trials of audio processing execution

36. Begin development of audio processing software

37. Begin training the machine learning algorithm with collected data

38. Test working version of audio processing software on Raspberry Pi

39. Develop a working audio processing software executable

40. MP4 Vs. Wav audio format Investigation 

41. Spectrogram audio investigation between transitions (Juypter Notebook)

42. Stiching together multiple audio files to see each respective transition overtime 

43. Automate generation of spectrogram images to be stored in database for deep learning image matching 
