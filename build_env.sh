#!/bin/bash
sudo apt-get update -y && sudo apt-get upgrade -y
#sudo apt install python3-venv -y

pyv="$(python3.8 -V 2>&1)"

if [ "$pyv" != 'Python 3.8.10' ]
then
    sudo apt-get install -y build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev tar wget vim
    wget https://www.python.org/ftp/python/3.8.10/Python-3.8.10.tgz
    sudo tar zxf Python-3.8.10.tgz
    cd Python-3.8.10
    sudo ./configure --enable-optimizations
    sudo make -j 4
    sudo make altinstall
    echo "alias python=/usr/local/bin/python3.8" | sudo tee -a /root/.bashrc >> /dev/null
    source /root/.bashrc
    sudo rm -rf /tmp/mutebox/Python*
fi

# Create MuteBox directory structure & Install modules
if [ ! -d /usr/local/bin/MuteBox ]
then
    sudo mkdir /usr/local/bin/MuteBox 
fi
if [ ! -d /usr/local/bin/MuteBox/Model ]
then
    sudo mkdir /usr/local/bin/MuteBox/Model 
fi
if [ ! -d /usr/local/bin/MuteBox/Audio-Software ]
then
    sudo mkdir /usr/local/bin/MuteBox/Audio-Software
fi

# Setup audio software
sudo cp -r /tmp/mutebox/Audio-Software /usr/local/bin/MuteBox
sudo cp -r /tmp/mutebox/CNN/spectrogram_predict.tflite /usr/local/bin/MuteBox/Model

# Create & Activate venv
#python3.8 -m venv /usr/bin/MuteBox/.venv/
#source /usr/bin/MuteBox/.venv/bin/activate

# Install dependencies
sudo apt install portaudio19-dev -y
sudo python3.8 -m pip install --upgrade pip
sudo python3.8 -m pip install librosa
sudo python3.8 -m pip install pyaudio 
sudo python3.8 -m pip install matplotlib
sudo python3.8 -m pip install numpy
sudo python3.8 -m pip install tensorflow
