# User Stories

1. As a user, I want MuteBox to mute the TV when a commercial is playing so that the noise of the commercial does not interrupt the people watching TV.

2. As a user, I want MuteBox to unmute the TV when the intended program resumes so that the people watching the program understand what is occuring.

3. As a user, I want MuteBox to mute the TV without input from the people watching so that they do not have to constantly watch for when commercials occur.

4. As a developer, I want the physical components of MuteBox to easily produced and replicated so that the cost of effort of making devices is managable.

5. As a developer, I want the physcial components of MuteBox to be as small as possible so that it does not take up much space.

6. As a developer, I want to implement a scheduler into MuteBox so that the device consistently checks for when a commerical is playing.