# Mutebox

## Abstract
Team Members:
- Trent Slavik
- Andrew Dillon
- Timoty Holstein

Project Advisor: Prof. Gallagher

Our goal is to build a replicable device which will accurately, consistently and quickly mute a television whenever an advertisement is playing, and unmute it when the advertisement concludes. This device should also work without user interaction once configured. In order to achieve this, this device will accomplish goals in different categories: physical specifications, response time, and usability.

Table of Contents:
1. [Project Description](https://gitlab.com/slavik.trent/mutebox/-/blob/main/Project-Description.md)
2. [User Stories](https://gitlab.com/slavik.trent/mutebox/-/blob/main/User_Stories.md)
3. [Test Plans](https://gitlab.com/slavik.trent/mutebox/-/tree/main/Class-Assignments/Test-Plan)
4. [User Manual](https://gitlab.com/slavik.trent/mutebox/-/wikis/User-Manual)
5. [PPT Slideshow](https://gitlab.com/slavik.trent/mutebox/-/blob/main/Class-Assignments/Powerpoint-Presentation/MuteBox-SlideShow-V2.pdf)
6. [Expo Poster](https://gitlab.com/slavik.trent/mutebox/-/blob/main/Class-Assignments/Poster/MuteBox_48x36_poster.pdf)
7. [Self-Assessment Essays](https://gitlab.com/slavik.trent/mutebox/-/tree/main/Class-Assignments/Individual-Capstone-Assessment)
8. [Summary of Hours](https://gitlab.com/slavik.trent/mutebox/-/wikis/Appendix#contribution-evidence)
8. [Summary of Expenses](https://gitlab.com/slavik.trent/mutebox/-/wikis/Budget)
9. [Appendix](https://gitlab.com/slavik.trent/mutebox/-/wikis/Appendix)
