import os
import matplotlib.pyplot as plt
import numpy as np
import librosa
import librosa.display
import glob

# Define the directory that contains the .mp4 files
input_directory = '/home/slaviktm/Cutups'

output_directory = '/home/slaviktm/Spectrograms'

# Use glob to find all .mp4 files in the directory
for category in os.listdir(input_directory):
    print("Converting " + category)
    for channel in os.listdir(input_directory + "/" + category):
        for date in os.listdir(input_directory + "/" + category + "/" + channel):
            for filename in glob.glob(os.path.join(input_directory + "/" + category + "/" + channel + "/" + date, '*.mp4')):
                print(filename)
                # Load the .mp4 file using librosa
                ad_to_ad, sr = librosa.load(filename)
                # Perform STFT on the audio data
                A = librosa.stft(ad_to_ad)
                S_db = librosa.amplitude_to_db(np.abs(A), ref=np.max)
                # Plot the spectrogram
                fig, ax = plt.subplots(figsize=(10, 5))
                img = librosa.display.specshow(S_db, x_axis='time', y_axis='log', ax=ax)

                # Save the spectrogram with a unique name
                name = filename.split("/")
                name_no_extension = name[-1].split(".")
                plt.savefig(output_directory + "/" + category + "/" + name_no_extension[0] + '_spec.png')
                plt.close()
           