import librosa
import librosa.display
import pyaudio
import matplotlib.pyplot as plt
import os
import numpy as np
import tensorflow as tf
from audio_stream import audio_stream

def getRemoteName(filepath):
        with open(filepath) as f:
            content = f.readlines()
            for line in content:
                if ('name  ' in line):
                        index =  line.index('name  ')
                        name = line[index + 6:-1]
        return name

msg = "------------------------------------\n \
       |  MuteBox Audio Procssing Filter  |\n \
       ------------------------------------\n \
       Running Audio Manager...\n"
print(msg)

files = os.listdir('/etc/lirc/lircd.conf.d/')
conf_file = [_ for _ in files if _[-5:] == ".conf"]
remoteName = getRemoteName('/etc/lirc/lircd.conf.d/' + conf_file[0])
muteCommand = 'irsend SEND_ONCE ' + remoteName + ' KEY_MUTE'

# Set up directories to save spectrograms
spectro_dir = './spectrograms'
if not os.path.exists(spectro_dir):
    os.makedirs(spectro_dir)

# Load the TFLite model and allocate tensors.
model_path="/usr/local/bin/MuteBox/Model/spectrogram_predict.tflite"
interpreter = tf.lite.Interpreter(model_path)
interpreter.allocate_tensors()

# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
print(output_details)

# Test the model on random input data.
input_shape = input_details[0]['shape']
print(input_shape)
# input_data = np.array(np.random.random_sample(input_shape), dtype=np.float32)

stream_obj = audio_stream(chunk_size=2048, sample_rate=44100)

# Main loop to capture and process audio every two
# while True:
for i in range(1):
    # Read a chunk of audio data from the input stream for two seconds
    audio_data = np.array([], dtype=np.float32)
    for i in range(int(stream_obj.sample_rate / stream_obj.chunk_size * 2)):
        audio_chunk = stream_obj.stream.read(stream_obj.chunk_size)
        audio_chunk = np.frombuffer(audio_chunk, dtype=np.float32)
        audio_data = np.concatenate((audio_data, audio_chunk))

    # Generate the spectrogram from the audio data using Librosa
    spectrogram = np.abs(librosa.stft(audio_data))
    
    # Normalize the spectrogram
    spectrogram = librosa.util.normalize(spectrogram)
    
    # Plot and save the spectrogram
    fig = plt.figure(figsize=[10, 5])
    librosa.display.specshow(librosa.amplitude_to_db(spectrogram, ref=np.max), y_axis='log', x_axis='time')
    plt.axis('off')
    plt.savefig(os.path.join(spectro_dir, f'spectrogram.png'), bbox_inches=None, pad_inches=0)
    plt.close(fig)

    # Convert saved spectrogram to tensor shape
    img = tf.keras.utils.load_img("./spectrograms/spectrogram.png", target_size=(200, 200))
    img_array = tf.keras.utils.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0) # Create a batch

    input_data = np.array(img_array, dtype=np.float32)
    interpreter.set_tensor(input_details[0]['index'], input_data)

    interpreter.invoke()

    # The function `get_tensor()` returns a copy of the tensor data.
    # Use `tensor()` in order to get a pointer to the tensor.
    output_data = interpreter.get_tensor(output_details[0]['index'])
    print("Output Data: ")
    print(output_data)
    print("\n")
    if (output_data):
        os.system(muteCommand)
