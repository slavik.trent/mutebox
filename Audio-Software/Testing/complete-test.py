import librosa
import librosa.display
import matplotlib.pyplot as plt
import os
import numpy as np
import tensorflow as tf
from ssh import pi_connect, pi_close
from live_analysis import audio_stream
import random

# Set up directories to save spectrograms
spectro_dir = './spectrograms'
if not os.path.exists(spectro_dir):
    os.makedirs(spectro_dir)

# Define the hostname (pi), username (pi), and private key file path (host)
hostname = '192.168.1.139'
username = 'user'
password = 'password'
# key_filename = 'C:\\Users\\slavi\\.ssh\\id_rsa.pem'

ssh = pi_connect(hostname, username, password)

if(ssh):
    print("Successfully Connected to Pi")    

# Load the TFLite model and allocate tensors.
interpreter = tf.lite.Interpreter(model_path=".\\test.tflite")
interpreter.allocate_tensors()

# Get input and output tensors.
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()
# print(output_details)

# Test the model on random input data.
input_shape = input_details[0]['shape']
# print(input_shape)
# input_data = np.array(np.random.random_sample(input_shape), dtype=np.float32)

stream_obj = audio_stream(chunk_size=2048, sample_rate=44100)
muteflag = 0
# Main loop to capture and process audio every two
# while True:
for i in range(1):
    # Read a chunk of audio data from the input stream for two seconds
    audio_data = np.array([], dtype=np.float32)
    for i in range(int(stream_obj.sample_rate / stream_obj.chunk_size * 2)):
        audio_chunk = stream_obj.stream.read(stream_obj.chunk_size)
        audio_chunk = np.frombuffer(audio_chunk, dtype=np.float32)
        audio_data = np.concatenate((audio_data, audio_chunk))
    
    # Generate the spectrogram from the audio data using Librosa
    spectrogram = np.abs(librosa.stft(audio_data))
    
    # Normalize the spectrogram
    spectrogram = librosa.util.normalize(spectrogram)
    
    # Plot and save the spectrogram
    fig = plt.figure(figsize=[10, 5])
    librosa.display.specshow(librosa.amplitude_to_db(spectrogram, ref=np.max), y_axis='log', x_axis='time')
    plt.axis('off')
    plt.savefig(os.path.join(spectro_dir, f'spectrogram.png'), bbox_inches=None, pad_inches=0)
    plt.close(fig)

    # Convert saved spectrogram to tensor shape
    img = tf.keras.utils.load_img("./spectrograms/spectrogram.png", target_size=(200, 200))
    img_array = tf.keras.utils.img_to_array(img)
    img_array = tf.expand_dims(img_array, 0) # Create a batch

    input_data = np.array(img_array, dtype=np.float32)
    interpreter.set_tensor(input_details[0]['index'], input_data)

    print("tflite model predicting ...")
    interpreter.invoke()

    # The function `get_tensor()` returns a copy of the tensor data.
    # Use `tensor()` in order to get a pointer to the tensor.
    output_data = interpreter.get_tensor(output_details[0]['index'])
    print("Printing Output Data: ", output_data)

    # 0 show_to_ad & 1 ad_to_ad & 2 ad_to_show
    muteflag = random.randint(0,2)
    if (muteflag == 0 or muteflag == 2):
        stdin, stdout, stderr = ssh.exec_command('irsend SEND_ONCE SAMSUNG_AA59-00600A KEY_MUTE')
        stdin.close()
        for line in stdout.readlines():
            print(line.strip().decode())

pi_close(ssh)
