import librosa
import librosa.display
import numpy as np
import pyaudio
import matplotlib.pyplot as plt
import os

# Set up PyAudio to capture audio from the default input device
chunk_size = 2048
sample_rate = 44100
audio_format = pyaudio.paFloat32
stream = pyaudio.PyAudio().open(
    format=audio_format,
    channels=1,
    rate=sample_rate,
    input=True,
    frames_per_buffer=chunk_size
)

# Set up directories to save spectrograms
spectro_dir = './spectrograms'
if not os.path.exists(spectro_dir):
    os.makedirs(spectro_dir)

# Main loop to capture and process audio every two seconds
i = 0
while True:
    # Read a chunk of audio data from the input stream for two seconds
    audio_data = np.array([], dtype=np.float32)
    for i in range(int(sample_rate / chunk_size * 2)):
        audio_chunk = stream.read(chunk_size)
        audio_chunk = np.frombuffer(audio_chunk, dtype=np.float32)
        audio_data = np.concatenate((audio_data, audio_chunk))
    
    # Generate the spectrogram from the audio data using Librosa
    spectrogram = np.abs(librosa.stft(audio_data))
    
    # Normalize the spectrogram
    spectrogram = librosa.util.normalize(spectrogram)
    
    # Plot and save the spectrogram
    fig = plt.figure(figsize=[10, 5])
    librosa.display.specshow(librosa.amplitude_to_db(spectrogram, ref=np.max), y_axis='log', x_axis='time')
    plt.axis('off')
   # plt.tight_layout()
    plt.savefig(os.path.join(spectro_dir, f'spectrogram.png'), bbox_inches=None, pad_inches=0)
    plt.close(fig)
    
    i += 1
