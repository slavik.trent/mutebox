import librosa
import librosa.display
import numpy as np
import pyaudio
import matplotlib.pyplot as plt
import os

# Set up PyAudio to capture audio from the default input device
class audio_stream():
    def __init__(self, chunk_size = 2048, sample_rate = 44100):
        self.chunk_size = chunk_size
        self.sample_rate = sample_rate
        self.audio_format = pyaudio.paFloat32
        self.stream = pyaudio.PyAudio().open(
            format=self.audio_format,
            channels=1,
            rate=self.sample_rate,
            input=True,
            frames_per_buffer=self.chunk_size
        )        

# NOT IN USE LOOK INTO
"""
    def get_audio_data(self):
        # Read a chunk of audio data from the input stream for two seconds
        audio_data = np.array([], dtype=np.float32)
        audio_chunk = self.stream.read(self.chunk_size)
        audio_chunk = np.frombuffer(audio_chunk, dtype=np.float32)
        audio_data = np.concatenate((audio_data, audio_chunk))
        
        return audio_data
    
    def normalize_audio_data(self, audio_data, spectro_dir):
        # Generate the spectrogram from the audio data using Librosa
        spectrogram = np.abs(librosa.stft(audio_data))
    
        # Normalize the spectrogram
        spectrogram = librosa.util.normalize(spectrogram)

        fig = plt.figure(figsize=[10, 5])
        librosa.display.specshow(librosa.amplitude_to_db(spectrogram, ref=np.max), y_axis='log', x_axis='time')
        plt.axis('off')
        plt.savefig(os.path.join(spectro_dir, f'spectrogram.png'), bbox_inches=None, pad_inches=0)
        plt.close(fig)

        return spectrogram
"""
