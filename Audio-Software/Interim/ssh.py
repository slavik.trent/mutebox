import paramiko
#import msvcrt
# import sys
# import termios
# import tty

def pi_connect(hostname, username, password):
    # Create a SSH client 
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname=hostname, username=username, password=password)
    return ssh

def pi_close(ssh):
    ssh.close()

